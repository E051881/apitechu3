package com.techu3.apitechu3.controllers;

import com.fasterxml.jackson.annotation.Nulls;
import com.sun.org.apache.xerces.internal.util.SynchronizedSymbolTable;
import com.techu3.apitechu3.Apitechu3Application;
import com.techu3.apitechu3.models.ProductModel;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@RestController
public class ProductController {

    static final String APIBaseUrl = "/apitechu/v1";

    @GetMapping(APIBaseUrl + "/products")
    public ArrayList<ProductModel> getProducts() {
        System.out.println("getProduct()");

        return Apitechu3Application.productModels;
    }

    @GetMapping(APIBaseUrl + "/products/{id}")
    public ProductModel getProductById(@PathVariable String id) {
        System.out.println("getProductById");
        System.out.println("La id del producto a obtener es " + id);

        ProductModel result = new ProductModel();

        for (ProductModel product : Apitechu3Application.productModels) {
            if (product.getId().equals(id)) {
                System.out.println("Producto encontrado");
                result = product;
            }
        }

        return result;
    }

    @PostMapping(APIBaseUrl + "/products")
    public ProductModel createProduct(@RequestBody ProductModel newProduct) {
        System.out.println("CreateProduct");
        System.out.println("la Id del producto a crear es " + newProduct.getId());
        System.out.println("la descripcion del producto a crear es " + newProduct.getDesc());
        System.out.println("El precio del producto a crear es " + newProduct.getPrice());

        Apitechu3Application.productModels.add(newProduct);

        return newProduct;
    }

    @PutMapping(APIBaseUrl + "/products/{id}")
    public ProductModel updateProduct(@RequestBody ProductModel product, @PathVariable String id) {
        System.out.println("updateProduct");
        System.out.println("la Id del producto a actualizar es " + product.getId());
        System.out.println("la descripcion del producto a actualizar es " + product.getDesc());
        System.out.println("El precio del producto a actualizar es " + product.getPrice());

        for (ProductModel productInList : Apitechu3Application.productModels) {
            if (productInList.getId().equals(id)) {
                productInList.setId(product.getId());
                productInList.setDesc(product.getDesc());
                productInList.setPrice(product.getPrice());
            }
        }
        return product;
    }

    @PatchMapping(APIBaseUrl + "/products/{id}")
    public ProductModel patchProduct(@RequestBody ProductModel productData, @PathVariable String id) {
        System.out.println("patchProduct");
        System.out.println("la Id del producto a actualizar patch es " + productData.getId());
        System.out.println("la descripcion del producto a actualizar patch es " + productData.getDesc());
        System.out.println("El precio del producto a actualizar patch es " + productData.getPrice());

        ProductModel result = new ProductModel();

        for (ProductModel productInList : Apitechu3Application.productModels) {
            if (productInList.getId().equals(id)) {
                System.out.println("Producto encontrado");
                result= productInList;

                if (productData.getDesc() != null) {
                   System.out.println("Actualizacion descripcion del producto");
                   productInList.setDesc(productData.getDesc());
                }

                if (productData.getPrice() > 0)
                    System.out.println("Actualizacion precio del producto");
                    productInList.setPrice(productData.getPrice());

            }
        }
        return result;
    }

    @DeleteMapping(APIBaseUrl + "/products/{id}")
    public ProductModel deleteProduct(@PathVariable String id) {
        System.out.println("deleteProduct");
        System.out.println("La id del producto a borrar es " + id);

        ProductModel result = new ProductModel();
        boolean foundCompany = false;

        for(ProductModel productInList : Apitechu3Application.productModels) {
            if (productInList.getId().equals(id)) {
                System.out.println("Producto encontrado");
                foundCompany = true;
                result = productInList;
            }
        }

        // Atención! no es necesario poner == true
        if (foundCompany == true) {
            System.out.println("Borrado producto");
            Apitechu3Application.productModels.remove(result);
        }

        return result;
    }
}