package com.techu3.apitechu3;

import com.techu3.apitechu3.models.ProductModel;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.ArrayList;
import java.util.Arrays;

@SpringBootApplication
public class Apitechu3Application {

	public static ArrayList<ProductModel> productModels;

	public static void main(String[] args) {
		SpringApplication.run(Apitechu3Application.class, args);

		Apitechu3Application.productModels= Apitechu3Application.getTestDate();
	}

	private static ArrayList<ProductModel> getTestDate() {

		ArrayList<ProductModel> productMolels = new ArrayList<>();

		productMolels.add(
				new ProductModel(
					"1"
					,"Producto 1"
					,10
				)
			);

		productMolels.add(
				new ProductModel(
						"2"
						,"Producto 2"
						,10
				)
			);

		productMolels.add(
				new ProductModel(
						"3"
						,"Producto 3"
						,10
				)
			);

		return productMolels;
	}
}
